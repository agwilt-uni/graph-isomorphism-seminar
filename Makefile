presentation.pdf: presentation.tex sources.bib
	pdflatex presentation.tex
	bibtex presentation
	pdflatex presentation.tex
	pdflatex presentation.tex

.PHONY: tidy
tidy:
	rm -fv *.aux *.log *.toc *.out *.bbl *.blg
