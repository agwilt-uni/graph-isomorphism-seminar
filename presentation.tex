\documentclass{beamer}
\usetheme{Madrid}

\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{graphicx}
\usepackage[ruled, vlined]{algorithm2e}
\usepackage{hyperref}

\renewcommand{\thealgocf}{}

\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Sym}{Sym}
\newcommand{\id}{\textup{id}}
\DeclareMathOperator{\im}{im}

\newtheorem{proposition}{Proposition}
\definecolor{RecapColour}{RGB}{0,128,0}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]

%\setbeamertemplate{section page}{%
%    \begingroup
%    \begin{beamercolorbox}[sep=10pt,center,rounded=true,shadow=true]{section title}
%        \usebeamerfont{sectiontitle}\the\beamer@tocsectionnumber.\thesection~\insertsection\par
%    \end{beamercolorbox}
%    \endgroup
%}

\AtBeginSection[]{
    \begin{frame}
        \vfill
        \centering
        \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
            \usebeamerfont{title}\insertsectionhead\par%
        \end{beamercolorbox}
        \vfill
    \end{frame}
}

\title[Graduate Seminar]{Isomorphism of Graphs of Bounded Valence Can be Tested in Polynomial Time}
\subtitle{Part I}
\author{Andreas Gwilt}

\begin{document}

\begin{frame}[plain]
    \maketitle
\end{frame}

\begin{frame}{Overview}
   \tableofcontents[pausesections,pausesubsections]
\end{frame}

\section{Preliminaries}

\subsection{Group Action Basics}

\begin{frame}{Basic Terminology}
   Let $G$ be a group and $\Omega$ a finite set.
   \begin{itemize}
      \item A \emph{group action} of $G$ on $\Omega$ is a group homomorphism
         $\phi : G \to \Sym(\Omega)$. Write $\sigma.x := (\phi(\sigma))(x)$.
      \item It is called \emph{faithful} if $\phi$ is injective.
      \item It is called \emph{transitive} if $\Omega$ consists of a single
         \emph{orbit} $G.x := \{\sigma.x\ |\ \sigma\in G\}$ for $x\in \Omega$.
      \item The \emph{stabiliser} of $x$ is
         $G_x := \{\sigma\in G\ |\ \sigma.x = x\}$.
         It is a subgroup of $G$ with index $|G.x|$.
      \item Let $x_1,\ldots,x_k \in \Omega$, $X:=\{x_1,\ldots,x_k\}$. Then
         \begin{itemize}
            \item $G_X := \{\sigma\in G\ |\ \sigma.X = X\} = \{\sigma\in G\ |\ \sigma.X \subset X \}$
               is the \emph{stabiliser} of $X$,
            \item $G_{(x_1,\ldots,x_k)} := G_{x_1}\cap\ldots\cap G_{x_k}$ is the
               \emph{pointwise stabiliser} of $X$.
         \end{itemize}
   \end{itemize}
\end{frame}

\begin{frame}{$G$-Blocks}
   Let $G$ act transitively on $\Omega$.
   \begin{itemize}
      \item A \emph{$G$-block} is a set $\emptyset\ne B\subsetneq\Omega$ such
         that $\forall \sigma,\tau\in G$, $\sigma.B$ and $\tau.B$ are equal or
         disjoint.
      \item The partition $\mathcal{B} := \{\sigma.B | \sigma\in G\}$ of $\Omega$
         is called a \emph{$G$-block system} in $\Omega$.
         \note{Mention induced action of $G$ on $\mathcal{B}$.}
      \item The action of $G$ on $\Omega$ is called \emph{primitive} if there are
         no blocks of size $>1$.
      \item A $G$-block system $\mathcal{B}$ is called \emph{minimal} if the
         induced action of $G$ on $\mathcal{B}$ is primitive.
         \note{i.e. No coarser block system containing \mathcal{B}}
   \end{itemize}
\end{frame}

\begin{frame}{Criterion for Primitivity}
   \begin{lemma}[without proof]\label{lem_block_bijection}
      Let $G$ act transitively on $\Omega$, and let $x\in\Omega$. Then there is a
      bijection
      \begin{align*}
         \{x\in B\subsetneq\Omega\textup{ block}\} &\overset{1:1}{\longleftrightarrow} \{G_x < H \lneq G\} \\
         B &\ \mapsto G_B\\
         H.x &\ \mapsfrom H
      \end{align*}
   \end{lemma}
   \pause
   \begin{corollary}\label{cor_primitivity_criterion}
      Let $G$, $\Omega$ and $x$ as above, $|\Omega|>1$. Then the following are
      equivalent:
      \begin{itemize}
         \item The action of $G$ on $\Omega$ is primitive.
         \item $\forall x\in\Omega$, $G_x<G$ maximal subgroup.
         \item $\exists x\in\Omega$, $G_x<G$ maximal subgroup.
         \qed
      \end{itemize}
   \end{corollary}
\end{frame}

\begin{frame}{Minimal Block Systems for $p$-Groups}
   \begin{lemma}[\cite{Luk82}, Lem. 1.1]\label{lem_p_group_blocks}
      Let $P$ be a $p$-group acting transitively on $\Omega$, $|\Omega|>1$.
      Let $\mathcal{B}$ be a minimal $P$-block system and
      $\phi : P \to \Sym(\Omega)$ the induced action of $P$ on $\mathcal{B}$.
      Consider the subgroup $P':=\ker\phi$ stabilising all blocks in
      $\mathcal{B}$. \\
      Then $|\mathcal{B}| = [P:P'] = p$.
      \pause
      \begin{proof}
         $P/P'$ is a $p$-group acting primitively on $\mathcal{B}$. \pause\\
         For $B\in\mathcal{B}$, $(P/P')_B < P/P'$ maximal subgroup by Corollary
         \ref{cor_primitivity_criterion}.
         Now maximal subgroups of finite $p$-groups are normal with index $p$
         (see \cite{Hun03}).
         Therefore, $|\mathcal{B}| = |(P/P').B| = [P/P':(P/P')_B] = p$. \pause\\
         Note that $P/P'$ acts faithfully on $\mathcal{B}$, i.e.
         $P/P' \cong \im\phi < \Sym(\mathcal{B})$.
         Now $|P/P'|=p^k$ divides $|\Sym(\mathcal{B})|=p! \Rightarrow |P/P'|=p$,
         since $P/P'$ not the trivial group.
      \end{proof}
   \end{lemma}
\end{frame}

\subsection{Some Basic Algorithms}

\begin{frame}{Nicely Describing a Permutation Group}
   \begin{itemize}
      \item Let $G<S_n$ given by generating set.
      \item Goal: Answer basic questions such as
         \begin{itemize}
            \item Determine $|G|$.
            \item Given a permutation $\sigma$, is $\sigma\in G$?
            \item Determine a generating set for a subgroup $H<G$ given by a
               polynomial time membership oracle.
         \end{itemize}
   \end{itemize}
\end{frame}

\begin{frame}{Nicely Describing a Permutation Group}
   Consider $G<\Sym(\{x_1,\ldots,x_n\})$ given by a generating set.
   \begin{itemize}
      \pause
      \item Let $G_i := G_{(x_1,\ldots,x_i)}$.
         $\leadsto G = G_0 > G_1 > \ldots > G_{n-1} = 1$
      \pause
      \item Algorithm will construct full sets $C_i$ of representatives for
         $G_i/G_{i+1}$.
         (Note that $|C_i| \le n-i$.)
      \pause
      \item Now $G = C_0G_1 \pause = C_0C_1\ldots C_{n-2}$.
   \end{itemize}
   \pause
   Schreier-Sims: Start with $C_0 = \ldots = C_{n-2} = \{1\}$, and keep expanding
   $C_i$s until $G\subset C_0C_1\ldots C_{n-2}$.
\end{frame}

\begin{frame}{Building the $C_i$}
   Let $C_0,\ldots,C_{n-2}$ be an incomplete set of representatives,
   $\alpha\in G$.
   \pause

   \begin{figure}
   \begin{algorithm}[H]
   \caption{Filter($\alpha$)}
      \KwResult{Ensures that $\alpha\in C_0C_1\ldots C_{n-2}$}
      \For{$i=0,\ldots,n-2$}{
         \uIf{$\gamma^{-1}\alpha\in G_{i+1}$ for some $\gamma\in C_i$}{
            $\alpha := \gamma^{-1}\alpha$\;
         }
         \Else{
            add $\alpha$ to $C_i$\;
            \Return\;
         }
      }
   \end{algorithm}
   \end{figure}

   \pause
   Note that \textbf{Filter($\alpha$)} even provides a representation
   $\alpha = \gamma_0 \gamma_1 \ldots \gamma_{n-2} \in C_0C_1\ldots C_{n-2}$.
\end{frame}

\begin{frame}{Which $\alpha\in G$ to Filter?}
   \begin{lemma}[from \cite{FHL80}]
      $G\subset C_0C_1\ldots C_{n-2}$ if and only if
      \begin{enumerate}
         \item The generating set of $G$ is contained in $C_0C_1\ldots C_{n-2}$, and
         \item every product of a pair of representatives is contained in
            $C_0C_1\ldots C_{n-2}$.
      \end{enumerate}
      \pause
      \begin{proof}
         Let $\sigma\in G$. Write $\sigma$ as a product of generators and write
         each generator as $\gamma_0\gamma_1\ldots\gamma_{n-2} \in C_0C_1\ldots C_{n-2}$.
         Now keep applying (2) to products $c_ic_j\in C_iC_j$ where $i\ge j$
         until $\sigma$ is of the required form.
         Note that $c_ic_j$ is contained in $C_jC_{j+1}\ldots C_{n-2}$, so this
         process ends in polynomial time.
      \end{proof}
   \end{lemma}
\end{frame}

\begin{frame}{The Schreier-Sims Algorithm}
   \begin{figure}
   \begin{algorithm}[H]
   \caption{Schreier-Sims}
      \KwIn{Group $G<S_n$ given by generators $\{\sigma_1,\ldots,\sigma_k\}$}
      \KwOut{Complete sets $C_i$ of representatives for $G_i/G_{i+1}$}
      $C_0 := C_1 := \ldots := C_{n-2} := \{1\}$\;
      \For{$i=0,\ldots,k$}{
         Filter($\sigma_k$)\;
      }
      \While{$\exists c_ic_j\in C_iC_j$ that hasn't been filtered yet}{
         Filter($c_ic_j$)\;
      }
      \Return{$(C_0,C_1,\ldots,C_{n-2})$}\;
   \end{algorithm}
   \end{figure}
\end{frame}

\begin{frame}
   \begin{proposition}[\cite{Hel17}, Ex. 2.1]
      Let $G<S_n$ be given by a generating set. Then we can achieve the following
      in polynomial time:
      \begin{enumerate}
         \item Determine $|G|$.
         \item Check if $\sigma\in S_n$ is contained in $G$.
         \item Given a subset $H<G$ with polynomial index and a polynomial time
            membership oracle, find a generating set for $H$.
         \item Given $\phi : G\to S_{n'}$, $n'$ polynomially bounded in $n$, and
            a subgroup $H<S_{n'}$, determine $\phi^{-1}(H)$.
      \end{enumerate}
   \end{proposition}
   \vspace{9em}
\end{frame}

\begin{frame}{More Basic Algorithms}
   \begin{itemize}
      \item Given $G<S_n$, we can determine the $G$-orbits.
         {\color{gray} (Connected components of $([n], \{\{i,\sigma.i\}\ |\ i\in[n], \sigma\in\ \textup{generators}\})$.)}
      \pause
      \item Given $G<S_n$ transitive, we can determine a minimal $G$-block
         system:
         \begin{itemize}
            \color{gray}
            \item Given $x\ne y\in [n]$, determine the smallest $G$-block
               containing $x$ and $y$, or decide that none exists.
               (Connected component of $a$ in
               $([n], \{\{\sigma.a,\sigma.b\}\ |\ \sigma\in G\})$.)
            \item Fix $x$ and see if there is a $y$ that shares a block with $x$.
            \item Then iterate until $G$ acts primitively on $\mathcal{B}$.
         \end{itemize}
      \pause
      \item Let $\mathcal{B}$ be a $G$-block system in $[n]$ and
         $\phi : G \to \Sym(\mathcal{B})$ be the induced action. Then we can
         determine $\ker\phi$ in polynomial time.
         {\color{gray} (Apply (4) from the last slide, with $H:=\{1\}$)}
   \end{itemize}
\end{frame}

\section{Isomorphism of Trivalent Graphs}

\subsection{Reduction to the Edge-Stabilising Automorphism Group $\Aut_e(X)$}

\begin{frame}{Reduction to Finding Generators for $\Aut_e(X)$}
   \begin{proposition}[\cite{Luk82}, Prop. 2.1]
      Testing isomorphism of trivalent graphs can be reduced to the problem of
      finding a generating set for $\Aut_e(X)$, where $X$ is a connected
      trivalent graph and $e \in E(X)$.
      \pause
      \begin{proof}
         Assume $X_1, X_2$ connected graphs. Consider the problem of finding an
         isomorphism sending $e_1\in E(X_1)$ to $e_2\in E(X_2)$. \\\pause
         \vspace{8em}
         $\Leftrightarrow$ automorphism $f\in\Aut_e(X)$ swapping the end points of $e$
      \end{proof}
   \end{proposition}
\end{frame}

\subsection{Approaching $\Aut_e(X)$ with chain $\pi_r : \Aut_e(X_{r+1}) \to \Aut_e(X_r)$}

\begin{frame}{Approaching $\Aut_e(X)$}
   \begin{itemize}
      \item Fix a connected trivalent graph $X$ with $n$ vertices, and $e\in E(X)$.
      \item Let $X_r$ be the union of all paths of length $\le r$ through $e$.
   \end{itemize}
   \pause
   \begin{align*}
      (e,\{e\})      &= &X_1\        &\subset    &X_2\        &\subset    &\ldots &\subset    &X_{n-1}\        &= &X\ \\
      \mathbb{Z}/(2) &= &\Aut_e(X_1) &\leftarrow &\Aut_e(X_2) &\leftarrow &\ldots &\leftarrow &\Aut_e(X_{n-1}) &= &\Aut_e(X)
   \end{align*}
   \begin{itemize}
      \item Group homomorphisms $\pi_r : \Aut_e(X_{r+1}) \to \Aut_e(X_r),\ f\mapsto f|_{X_r}$
      \pause
      \item Assuming we know $\Aut_e(X_r)$, we can get $\Aut_e(X_{r+1})$ by
         finding generating sets for:
         \begin{itemize}
            \item $\ker\pi_r < \Aut_e(X_{r+1})$
            \item $\im\pi_r < \Aut_e(X_r)$
         \end{itemize}
         (Let $\mathcal{K}$ and $\mathcal{I}$ be such generating sets, and let
         $\mathcal{I}'\subset\Aut_e(X_{r+1})$, $\pi_r(\mathcal{I}')=\mathcal{I}$.
         Then $\Aut_e(X_{r+1})$ is generated by $\mathcal{I'}\cup\mathcal{K}$.)
         \note{\cite{Luk82}, Prop. 2.3. shows us how to construct the preimages.}
   \end{itemize}
\end{frame}

\subsubsection{Determining $\ker\pi_r$}

\begin{frame}{Finding Generators for $\ker\pi_r$}
   \begin{itemize}
      \item Each $v\in V(X_{r+1})\setminus V(X_r)$ is connected to 1, 2 or 3
         vertices from $X_r$.
      \pause
      \item Let $A := \{S\subset V(X_r)\ |\ 1\le|S|\le3\}$.
      \item Define $f:V(X_{r+1})\setminus V(X_r) \to A,\ v\mapsto \Gamma_X(v)\cap V(X_r)$.
      \item Example (picture taken from \cite{Luk82}):
         \begin{figure}
            \includegraphics{example_from_luks.png}
         \end{figure}
      \item Here, $f(v_1) = \{w_1\}$, $f(v_2) = \{w_1, w_2\}$,
         $f(v_3) = f(v_4) = \{w_3, w_4, w_4\}$.
      \pause
      \item Note that for $a\in A$, $|f^{-1}(a)|\in\{0,1,2\}$.
      \item Call $v\ne v'$ \emph{twins} if $f(v) = f(v')$.
   \end{itemize}
\end{frame}

\begin{frame}{Finding Generators for $\ker\pi_r$}
   Note that $v\in V(X_{r+1})\setminus V(X_r), \sigma\in\Aut_e(X_{r+1})
   \Rightarrow f(\sigma(v)) = \sigma(f(v))$.
   \pause
   \begin{lemma}
      $\ker\pi_r$ is the $2$-group generated by transpositions of pairs of twins.
      \begin{proof}
         \begin{itemize}
            \item[``$\subset$'':] Now let $\sigma\in \ker\pi_r$.
               Then $f(\sigma(v)) = \sigma(f(v)) = f(v)$, so $\sigma(v) = v$ or
               $v$ and $\sigma(v)$ are twins.
            \item[``$\supset$'':] Let $v,v'$ twins. Then $(v,v')$ compatible
               with the edges to $f(v)$. \qedhere
         \end{itemize}
      \end{proof}
   \end{lemma}
   \pause
   \begin{theorem}[Tutte, Prop. 2.2 in \cite{Luk82}]
      For each $r$, $\Aut_e(X_r)$ is a $2$-group.
      \begin{proof}
         $\Aut_e(X_1) = \mathbb{Z}/(2)$ is a $2$-group, and
         $|\Aut_e(X_{r+1})| = |\im\pi_r||\ker\pi_r|$ $\Rightarrow$ $\Aut_e(X_{r+1})$
         is a $2$-group by induction.
      \end{proof}
   \end{theorem}
\end{frame}

\subsubsection{Determining $\im\pi_r$}

\begin{frame}{Finding Generators for $\im\pi_r$}
   \begin{itemize}
      \item Let $A_i := \{a\in A\ |\ |f^{-1}(a)| = i \}$.
         Then $A = A_0 \overset{.}{\cup} A_1 \overset{.}{\cup} A_2$.
      \item Let $A' := A \cap E(X_{r+1})$.
   \end{itemize}
   \pause
   \begin{proposition}[\cite{Luk82}, Prop. 2.3]
      $\im\pi_r$ is precisely the set of $\sigma\in\Aut_e(X_r)$ stabilising
      $A'$ and the $A_i$.
      \pause
      \begin{proof}
         ``$\subset$'': Not hard to check using properties of $\sigma$ and $f$.

         ``$\supset$'': Let $\sigma\in\Aut_e(X_r)$ stabilising $A'$,$A_i$.
         $\leadsto$ extend $\sigma$ to $\Aut_e(X_{r+1})$:
         \begin{itemize}
            \item Let $v, f(v)\in A_1$. Then $\sigma(f(v))\in A_1$, so map $v$
               to $f^{-1}(\sigma(f(v)))$.
            \item Let $v, v'$ twins, {\color{RecapColour}i.e. $f(v) = f(v') \in A_2$}.
               Then map $v$ and $v'$ to the two vertices $f^{-1}(\sigma(f(v)))$
               in either order.
         \end{itemize}
         By construction, this is compatible with the edges between $V(X_r)$ and
         $V(X_{r+1})\setminus V(X_r)$.
         Compatibility with the edges in $E(X_{r+1})\setminus E(X_r)$ between
         vertices of $V(X_r)$ is exactly the fact that $\sigma(A') = A'$.
      \end{proof}
   \end{proposition}
\end{frame}

\begin{frame}{Reduction to Colour Preserving Subgroup of $2$-Group}
   Now partition $A$ into six colours:
   \[ A_0 \cap A',\ \ A_1 \cap A',\ \ A_2 \cap A',\ \ A_0 \setminus A',\ \ A_1 \setminus A',\ \ A_2 \setminus A' \]
   Goal: Find the colour-preserving elements of $\Aut_e(X_r)$ in its action on
   $A$.
   \pause
   \begin{problem}[Colour Preserving Subgroup of $2$-Group]
      \begin{description}
         \item[Input:] A set of generators for a $2$-subgroup $G<\Sym(A)$, where $A$
            is a coloured set.
         \item[Find:] A set of generators for the subgroup
            $\{\sigma\in G\ |\ \sigma\ \textup{is colour preserving}\}$.
      \end{description}
   \end{problem}
   \pause
   Approach: Divide and conquer.

   $\leadsto$ Generalise problem to allow recursion.
\end{frame}

\subsection{The Colour Automorphism Problem for 2-Groups}

\begin{frame}{Generalising the Problem}
   \begin{itemize}
      \item Fix a coloured set $A$ with $n$ elements. Write $a\sim b$ for ``$a$ has the same colour as $b$''.
      \item For $B\subset A$, $K\subset \Sym(A)$, define
         $\mathscr{C}_B(K) := \{\sigma\in K\ |\ \forall b\in B: \sigma(b)\sim b\}$.
      \item Note that
         \begin{itemize}
            \item $\mathscr{C}_B(K \cup K') = \mathscr{C}_B(K) \cup \mathscr{C}_B(K')$.
            \item $\mathscr{C}_{B\cup B'}(K) = \mathscr{C}_B\mathscr{C}_{B'}(K)$.
         \end{itemize}
   \end{itemize}
   \begin{problem}[Colour Preserving Coset Problem]
      \begin{description}
         \item[Input:] A set of generators for a $2$-subgroup $G<\Sym(A)$, a
            $G$-stable subset $B\subset A$, and $\sigma\in\Sym(A)$.
         \item[Find:] $\mathscr{C}_B(\sigma G)$.
      \end{description}
   \end{problem}
\end{frame}

\begin{frame}{The Colour Preserving Coset Problem}
   \begin{problem}[Colour Preserving Coset Problem]
      \begin{description}
         \item[Input:] A set of generators for a $2$-subgroup $G<\Sym(A)$, a
            $G$-stable subset $B\subset A$, and $\sigma\in\Sym(A)$.
         \item[Find:] $\mathscr{C}_B(\sigma G)$.
      \end{description}
   \end{problem}
   \pause
   \vfill
   \begin{lemma}[\cite{Luk82}, Lem. 2.4]
      If $\mathscr{C}_B(\sigma G)$ is not empty then it is a left coset of the
      subgroup $\mathscr{C}_B(G)$.
      \begin{proof}
         $B$ is $G$-stable $\Rightarrow$ $\mathscr{C}_B(G)$ subgroup.
         Let $\sigma_0\in\mathscr{C}_B(\sigma G)$.
         Then $\mathscr{C}_B(\sigma G) = \mathscr{C}_B(\sigma_0 G) =
         \sigma_0\mathscr{C}_B(G)$.
      \end{proof}
   \end{lemma}
\end{frame}

\begin{frame}{Divide and Conquer}
   \begin{itemize}
      \item If $|B| = 1$, let $B = \{b\}$. Then
         \[ \mathscr{C}_B(\sigma G) =
            \{ \sigma g \in \sigma G\ |\ \sigma.g.b \sim b \} =
            \begin{cases}
               \sigma G  &\textup{if}\ \sigma(b)\sim b \\
               \emptyset &\textup{else}
            \end{cases} \]
      \pause
      \item Else if $B = B' \overset{.}{\cup} B''$ union of $G$-stable subsets, then
         $\mathscr{C}_B(\sigma G) = \mathscr{C}_{B''}\mathscr{C}_{B'}(\sigma G)$.
      \pause
      \item Otherwise, note that $G$ acts transitively on $B$.
         The following computations can all be performed in polynomial time:
         \pause
         \begin{itemize}
            \item Let $\{B',B''\}$ be a minimal block system.
            \pause
            \item Let $H$ be the subgroup of $G$ stabilizing $B'$, $B''$,
               $G = H \cup \tau H$.
               \begin{align*}
                  \Rightarrow\ \mathscr{C}_B(\sigma G)
                  &= \mathscr{C}_B(\sigma H) \cup \mathscr{C}_B(\sigma \tau H) \\
                  &= \mathscr{C}_{B''}\mathscr{C}_{B'}(\sigma H) \cup \mathscr{C}_{B''}\mathscr{C}_{B'}(\sigma\tau H)
               \end{align*}
            \pause
            \item If $\mathscr{C}_B(\sigma H)$, $\mathscr{C}_B(\sigma \tau H)$
               both non-empty:
               \begin{itemize}
                  \item Let $\mathscr{C}_B(\sigma H) = \rho_1\mathscr{C}_B(H)$,
                     $\mathscr{C}_B(\sigma \tau H) = \rho_2\mathscr{C}_B(H)$.
                  \item Then $\mathscr{C}_B(\sigma G) = \rho_1<\mathscr{C}_B(H), \rho_1^{-1}\rho_2>$.
               \end{itemize}
         \end{itemize}
   \end{itemize}
\end{frame}

\subsection{Summary}

\begin{frame}{Summary of the Trivalent Graph Isomorphism Algorithm}
   For each $e_1\in X_1$ and $e_2\in X_2$ try the following:

   \vfill

   Find a list of generators for $\Aut_e(X)$:
   \begin{itemize}
      \item Determine $\Aut_e(X_1) = \Sym(e)$
      \item For $r = 2,\ldots,n-1$:
      \begin{itemize}
         \item Determine $A := \{ S \subset V(X_r)\ |\ 1 \le |S| \le 3 \}, A_0, A_1, A_2$ and $A'$.
         \item Determine $\mathcal{K} := \{ (v,v')\ |\ v\ne v', f(v)=f(v')\in A_2 \}$ generating set for $\ker\pi_r < \Aut_e(X_{r+1})$.
         \item Determine generators $\mathcal{I}$ for
            $\im\pi_r =
            \{ \sigma\in\Aut_e(X_r)\ |\ \sigma$ stabilises $A_0\cap A', A_1\cap A', A_2\cap A', A_0\setminus A', A_1\setminus A', A_2\setminus A' \}
            = \mathscr{C}_A(\Aut_e(X_r))$:
            \begin{itemize}
               \item Run recursive algorithm from last slide for
                  $G := \Aut_e(X_r)$, $B := A$, $\sigma = \id$.
            \end{itemize}
         \item Lift $\mathcal{I} \subset \Aut_e(X_r)$ to $\mathcal{I'} \subset \Aut_e(X_{r+1})$.
         \item Now $\mathcal{K} \cup \mathcal{I'}$ is a generating set for $\Aut_e(X_{r+1})$.
      \end{itemize}
   \end{itemize}
   If there is a generator for $\Aut_e(X)$ swapping the endpoints of $e$, then
   the graphs we started with have an isomorphism sending $e_1$ to $e_2$.
\end{frame}

\begin{frame}{References}
   \bibliographystyle{alpha}
   \bibliography{sources.bib}
\end{frame}

\end{document}
